def myzip (*args):
    r = []
    for j in range(len(max(args))):
        l = []
        for i in range(len(args)):
            l.append(args[i][j])
        r.append(l)
    return r




def mymap (f,*args):
    z = myzip(*args)
    r = []
    for i in z:
        r.append(f(*i))
    return r

def myfilter (f,col):
    r = []
    for i in col:
        if f(i):
            r.append(i)
    return r



list1 = [i for i in range(10)]
list2 = [i for i in range(5,15)]
list3 = [i for i in range(10,20)]
print(myzip(list3,list2,list1))
print(mymap(lambda *x: (sum(x)),list1,list2,list3))
print([*(myfilter(lambda x:x%2, list1))])